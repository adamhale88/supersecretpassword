// =========== Globally decalring our varaibles.====================================================================//
var lowCase = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
// console.log(lowCase)
var upCase = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
// console.log(upCAse)
var num = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
// console.log(num)
var symb = ["+", "/", "!", "@", "$", "%", "#", "*"];
// console.log(symb)
var password = "" 
// console.log(password)
var passwordText = ""
// console.log(passwordText)
//=================================================================================//

//------ Because we are calling the function generatePassword in the funtion of writePassword below, we are making our function to do exactly that. generate the password based on user input. then pass that to the writePassword function to push it to the UI.-----//

//-----Generating our User Input Based, Randomized Password. ------------------------//
// ----Must be 8-128 characters. of any order,of any choice. ------------------------//

function generatePassword() {
//console.log(generatePassword)

//-------We prompt the user for data, ---------------------------------------------------------//
//-------in this case its based on lower and Uppercase Alphabet, 0-9 numeric, and a few developer chosen symbols 
//--------------------------------------------------------------------------------------------------------------------//
  var newArray = []
//console.log(newArray)
  var userLength = prompt("Please pick your total password length. Must be 8-128 characters")
//console.log(userLength)
  var userLow = confirm("Would you like to include lowercase letters? Okay for Yes. Cancel for No")
//console.log(userLow)
  var userUp = confirm("Would you like to include Uppercase letters? Okay for Yes Cancel for No")
//console.log(userUp)
  var userSym = confirm("Would you like to include Symbols? Okay for Yes Cancel for No")
//console.log(usersym)
  var userNum = confirm("Would you like to include Numbers? Okay for Yes Cancel for No")
//consoloe.log(userNum)

//-------we use if statements to check the valid user choices, -------------------------//
//-------then push the chosen arrays into the empty array newArray. --------------------//
//-------Using the .concat array method. -----------------------------------------------//
    if (userLow) {
      newArray = newArray.concat(lowCase);
    }
//console.log(newArray)
    if (userUp) {
      newArray = newArray.concat(upCase)
    }
//console.log(newArray)    
    if (userSym) {
      newArray = newArray.concat(symb);
    }
//console.log(newArray)
    if (userNum) {
      newArray = newArray.concat(num);
     
    }
//console.log(newArray) 

//-------By using a For loop we can take the users password length choice and loop each array that amount of times 
//-------this lets us grab that equaL amount of charcters. -------------------------//
//-------using our math.floor and math.random methods we are able to floor our index and choose randomly only within our given arrays--------////
     for (i = 0; i < userLength; i++){
    var randomNumber = Math.floor(Math.random()* newArray.length)
    password += newArray[randomNumber]
       
  }
//console.log(newArray)
  return password;
 //console.log(password)
  }

var generateBtn = document.querySelector("#generate");

// Write password to the #password input
function writePassword() {
  var passwordText = document.querySelector("#password");
  generatePassword();
  passwordText.value = password;
  password = '' // to clear out the textfeild if a user has already generated a password prior
}

// Add event listener to generate button
generateBtn.addEventListener("click", writePassword,);