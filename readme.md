
# 📖  Super Secret Password Generator 

<br>

## 🏆 https://adamhale88.github.io/supersecretpassword/## 🏆

<br>
 <img src="Assets\myworkingscreenshot.png"/>
&nbsp;   
&nbsp;   
&nbsp;  

## Languages:
---
 
|Javascript|
|-|

&nbsp;   

## 📝 Project Criteria

- [X] Generate a password for the user

- [X] Must get password from the user via prompt or checkboxes

- [X] User should have the option to set the password Length from 8-128 characters.

- [X] User is then given the choice to select from a variety of character options 
    * Lowercase Letters a - z

    * Uppercase Letters A - Z

    * Numbers 0-9

    * Symbols

- [X] Password should be presented to the user after prompts have been selected and the genrate button has been clicked. 

&nbsp;   
&nbsp;   





